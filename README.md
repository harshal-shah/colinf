# README #

Repo for monitoring POC using collectd influxdb and grafana.


### How do I get set up? ###

* Download the repo
* Run Vagrant up
* This will create 4 machines, 3 machines will have collectd, influxdb in cluster mode and one machine will have grafana
* Open grafana using URL : http://192.168.17.99:3000
* Go to Dashboards > Home > Import
* Navigate to templates folder and import the FreeMemoryDashboard.json
* This will show free memory on all 3 machines.
* memload.pl can be used on any of the machines to simulate load on memory

#
# Tech debts 
#

* Need a better way to import dashboards via Grafana API