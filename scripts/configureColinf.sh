#!/bin/bash

#
# Variables
#

HOST=$(hostname);

#
# Adding host entries
#
sudo /vagrant/scripts/appendHosts.sh

#
# Configuring collectd and influxdb
#
sudo cp /tmp/collectd-5.5.0/src/types.db /opt/collectd/share/collectd/types.db
sudo mv /opt/collectd/etc/collectd.conf /opt/collectd/etc/collectd.conf_orig
sudo cp /vagrant/templates/collectd.conf.tmpl /opt/collectd/etc/collectd.conf
sudo mv /etc/influxdb/influxdb.conf /etc/influxdb/influxdb.conf_orig
sudo cp /vagrant/templates/influxdb.conf.tmpl /etc/influxdb/influxdb.conf
sudo sed -i "s|%HOSTNAME%|$HOST|g" /opt/collectd/etc/collectd.conf /etc/influxdb/influxdb.conf
case $HOST in
		colinf1.hs.com )
		    INFOPT="";;
		colinf2.hs.com )
		    INFOPT="INFLUXD_OPTS=-join colinf1.hs.com:8088";;
		colinf3.hs.com )
		    INFOPT="INFLUXD_OPTS=-join colinf1.hs.com:8088,colinf2.hs.com:8088";;
esac
sudo echo "$INFOPT" >> /etc/default/influxdb
sudo rm -rf /var/lib/influxdb/meta/*
# Disable firewall in centos 7
# else VM's cant communicate
sudo systemctl stop firewalld.service 
sudo systemctl disable firewalld.service
sudo service influxdb start
sudo service collectd start
#
# Test Command 
#
#curl -G http://colinf1.hs.com:8086/query?pretty=true --data-urlencode "db=collectd_db" --data-urlencode "q=show series"
